-- This is a DDL Schema for PostgreSQL 10.2
-- created by Seth Moeckel <seth@eistudios.org>.
-- USE CAUTION WHEN EDITING THIS SCHEMA!

-- +goose NO TRANSACTION
-- +goose Up
CREATE DATABASE identacourse;

-- +goose NO TRANSACTION
-- +goose Up
CREATE EXTENSION IF NOT EXISTS citext;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS pg_trgm;

-- +goose Up
CREATE TABLE users (
	-- id type here
	uuid	uuid DEFAULT uuid_generate_v4(),
	PRIMARY KEY (uuid),
	access_level	smallint not null,

  deleted boolean not null,
  deletion_msg text,

	email	citext not null unique,
	passwd_hash text not null,

	fname 	text not null,
	lname 	text not null,

	created_at	timestamptz not null,
	last_mod_at	timestamptz not null,

	allow_location_services boolean not null,
	allow_browser_notifications boolean not null,
	allow_interest_notification boolean not null,
	session_lifespan text
);

CREATE INDEX CONCURRENTLY ON users(uuid, email);

-- +goose Up
CREATE TABLE tags (
	uuid uuid DEFAULT uuid_generate_v4(),
  PRIMARY KEY (uuid),

  name citext not null,
  type citext not null,
	owner_id text not null,

  created_at timestamptz not null,
  last_mod_at timestamptz not null
);

-- +goose Up
CREATE TABLE courses (
	uuid	uuid DEFAULT uuid_generate_v4(),
	PRIMARY KEY (uuid),

  created_by text not null,
	cname text not null,

	description	text,
	tags	text[],

  click_link text not null,
	offered_by text not null,
	-- ood -> out of date

  flagged_ood boolean not null,
  flaggers text[],

	-- location_lat float not null,
	-- location_lng float not null,

  created_at timestamptz not null,
  last_mod_at timestamptz not null
);

CREATE INDEX CONCURRENTLY ON courses(uuid, cname, created_by);

CREATE INDEX CONCURRENTLY index_courses_trigram
ON courses
USING gin (cname gin_trgm_ops);

-- CREATE TABLE notifications (
-- 	uuid uuid DEFAULT uuid_generate_v4(),
-- 	PRIMARY KEY (uuid),
--
-- 	subject_id text not null,
-- 	click_link text not null,
--
-- 	title text not null,
-- 	body text not null,
--
-- 	created_at timestamptz not null,
-- 	viewed boolean not null
-- );

-- CREATE TABLE jwt_tokens (
--   uuid uuid DEFAULT uuid_generate_v4(),
--   PRIMARY KEY (uuid)
--
-- 	subject_id text not null,
-- 	token_hash text not null,
--
-- 	created_at timestamptz not null,
-- 	last_mod_at timestamptz not null
-- );

-- should basically be a time series table
-- that stores the number of redirects/timeframe
-- use the GROUP BY and ORDER BY commands
-- to aggregate
CREATE TABLE redirect_metrics (
  course_id text not null,
	user_id text,
	time timestamptz not null
);

-- +goose Down
-- DROP TABLE subscribers, users, records, tags, courses;
