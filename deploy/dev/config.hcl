http {
  listen_addr = ":8081"

  allow_creds = true
  debug = true

  allow_origins = [
    "http://localhost:8080"
  ]

  allow_methods = [
    "GET",
    "PUT",
    "POST",
    "DELETE",
  ]
}

logger {
  mode_json = false
  mode_debug = true
}

mailgun {
  sender = "noreply@eistudios.org"
  url = "eistudios.org"
  key = "key-example"
}

database {
  url = "mongodb://localhost:27017"
}

auth {
  key = "bananasAreRad"
}
