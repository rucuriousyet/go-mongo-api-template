INSERT INTO users (
  access_level,
  email,
  passwd_hash,
  fname,
  lname,
  created_at,
  last_mod_at
) VALUES (
  0,
  'seth@eistudios.org',
  'passwd_hash_example',
  'seth',
  'moeckel',
  now(),
  now()
);

INSERT INTO users (
  access_level,
  email,
  passwd_hash,
  fname,
  lname,
  created_at,
  last_mod_at
) VALUES (
  0,
  'shireen@identacourse.com',
  'passwd_hash_example',
  'shireen',
  'firozean',
  now(),
  now()
);
