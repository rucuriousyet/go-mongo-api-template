http {
  listen_addr = ":8081"

  allow_creds = true
  debug = false

  allow_origins = [
    "https://identacourse.com"
  ]

  allow_methods = [
    "GET",
    "PUT",
    "POST",
    "DELETE",
  ]
}

logger {
  mode_json = true
  mode_debug = false
}

mailgun {
  sender = "noreply@eistudios.org"
  url = "eistudios.org"
  key = "key-example"
}

database {
  url = "postgres://user:password@localhost:5432/identacourse?sslmode=disable"
}

auth {
  key = "bananasAreRad"
}
