package router

import (
	"net/http"

	"github.com/rs/cors"
	"github.com/rs/zerolog"
	goji "goji.io"
	"goji.io/pat"

	"gitlab.com/rucuriousyet/go-mongo-api-template/config"
	"gitlab.com/rucuriousyet/go-mongo-api-template/controllers"
	"gitlab.com/rucuriousyet/go-mongo-api-template/middlewares"
	"gitlab.com/rucuriousyet/go-mongo-api-template/service"
)

func GenerateHandlers(conf config.Configuration, db *service.DBService, email *service.EmailService, logger zerolog.Logger) http.Handler {
	// create new root router with recovery middleware
	root := goji.NewMux()
	root.Use(middlewares.Recover(logger))
	root.Use(middlewares.Time(logger))

	// healthcheck returns config and startup info
	root.Handle(pat.Get("/health"), controllers.HealthCheck(conf))

	// cors middleware
	root.Use(cors.New(cors.Options{
		AllowedOrigins:   conf.HTTP.AccessControlAllowOrigin,
		AllowCredentials: conf.HTTP.AccessControlAllowCreds,
		AllowedMethods:   conf.HTTP.AccessControlAllowMethods,
		Debug:            conf.HTTP.AccessControlDebug,
	}).Handler)

	return root
}
