FROM scratch
MAINTAINER Seth Moeckel <smeck1999@gmail.com>

ENV DEV false
ENV CONF_FILE /config.hcl
EXPOSE 8080

COPY bin/apiserver /
COPY deploy/prod/config.hcl /config.hcl

ENTRYPOINT ["/apiserver --config=$CONF_FILE"]
