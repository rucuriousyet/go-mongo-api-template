package controllers

import (
	"encoding/json"
	"net/http"

	cfg "gitlab.com/rucuriousyet/go-mongo-api-template/config"
)

func HealthCheck(config cfg.Configuration) http.Handler {
	stats, err := json.Marshal(config)
	if err != nil {
		panic(err)
	}

	return http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
		_, err := resp.Write(stats)
		if err != nil {
			http.Error(resp, "Failed to write response", 500)
		}
	})
}
