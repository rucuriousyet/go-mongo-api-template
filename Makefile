run:
	DEV=true go run main.go --config=deploy/dev/config.hcl

setup_db:
	echo "nothing to do."

build:
	mkdir -p bin
	CGO_ENABLED=0 go build -o bin/apiserver main.go
