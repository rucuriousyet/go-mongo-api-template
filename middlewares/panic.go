package middlewares

import (
	"net/http"
	"net/http/httputil"

	"github.com/rs/zerolog"
)

func Recover(logger zerolog.Logger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			defer func() {
				if err := recover(); err != nil {
					dump, _ := httputil.DumpRequest(req, true)

					logger.Error().Err(err.(error)).Str("request", string(dump)).Msg("caught panic!")
					http.Error(res, err.(error).Error(), 500)
				}
			}()

			next.ServeHTTP(res, req)
		})
	}
}
