package middlewares

import (
	"net/http"
	"time"

	"github.com/rs/zerolog"
)

func Time(logger zerolog.Logger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			start := time.Now()
			next.ServeHTTP(res, req)

			end := time.Now()
			logger.Debug().
				Str("reqtime", end.Sub(start).String()).
				Str("method", req.Method).
				Str("route", req.URL.String()).
				Msg("timed request")
		})
	}
}
