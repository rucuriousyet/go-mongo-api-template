package cmd

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/rucuriousyet/go-mongo-api-template/config"
	"gitlab.com/rucuriousyet/go-mongo-api-template/router"
	"gitlab.com/rucuriousyet/go-mongo-api-template/service"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "api.GoMongoAPITemplate.com",
	Short: "GoMongoAPITemplate API",

	Run: func(cmd *cobra.Command, args []string) {

		// read in and setup our config
		conf, err := config.GetConfig(cfgFile)
		if err != nil {
			log.Fatalln(err)
		}

		// grab the logger
		logger := config.GetLogger()
		logger.Info().Msg("setting up GoMongoAPITemplate api services...")

		// setup the database service
		db, err := service.NewDBService(conf)
		if err != nil {
			logger.Fatal().Err(err).Interface("conf", conf.Database).Msg("failed to setup database service")
		}

		// setup the email service
		email, err := service.NewEmailService(conf, logger)
		if err != nil {
			logger.Fatal().Err(err).Interface("conf", conf.Mailgun).Msg("failed to setup email service")
		}

		// create routes handlers using controllers and services
		routes := router.GenerateHandlers(conf, db, email, logger)
		logger.Info().Interface("config", conf).Msg("starting GoMongoAPITemplate api listener")

		// start the HTTP listener
		err = http.ListenAndServe(conf.HTTP.ListenAddress, routes)
		if err != nil {
			logger.Fatal().Err(err).Msg("failed to start HTTP listener")
		}
	},
}

func Execute() {

	// grab flag for config file location
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "config.hcl", "config file (default is ./config.hcl)")

	// start the application
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
