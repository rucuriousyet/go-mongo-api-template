package service

import (
	"errors"

	"github.com/rs/zerolog"
	mailgun "gopkg.in/mailgun/mailgun-go.v1"

	"gitlab.com/rucuriousyet/go-mongo-api-template/config"
)

type EmailService struct {
	mailer        mailgun.Mailgun
	logger        zerolog.Logger
	defaultSender string
}

func NewEmailService(conf config.Configuration, logger zerolog.Logger) (*EmailService, error) {
	return &EmailService{
		mailer:        mailgun.NewMailgun(conf.Mailgun.URL, conf.Mailgun.Key, ""),
		logger:        logger,
		defaultSender: conf.Mailgun.Sender,
	}, nil
}

func (e *EmailService) DefaultSender() string {
	return e.defaultSender
}

func (e *EmailService) Send(subject, from, msg string, to ...string) (string, error) {
	message := e.mailer.NewMessage(
		from,
		subject,
		msg,
		to..., // variadic
	)

	// send the new email
	mresp, id, err := e.mailer.Send(message)
	if err != nil {
		return "", errors.New("Failed to send email to new subscriber: " + err.Error() + "/" + mresp)
	}

	// log the new send
	e.logger.Debug().
		Str("email-id", id).
		Interface("send-to", to).
		Str("mailgun-resp", mresp).
		Msg("sent email")

	return id, nil
}
