package service

import (
	"gitlab.com/rucuriousyet/go-mongo-api-template/config"
	mgo "gopkg.in/mgo.v2"
)

type DBService struct {
	session *mgo.Session
}

func NewDBService(conf config.Configuration) (*DBService, error) {
	db, err := mgo.Dial(conf.Database.URL)
	if err != nil {
		return nil, err
	}

	return &DBService{db}, nil
}
