package config

import (
	"io/ioutil"

	"github.com/hashicorp/hcl"
)

type ConfigFile struct {
	Server Configuration `hcl:"api"`
}

func ReadFile(filepath string) (Configuration, error) {
	data, err := ioutil.ReadFile(filepath)
	if err != nil {
		return Configuration{}, err
	}

	conf := Configuration{}
	err = hcl.Decode(&conf, string(data))
	if err != nil {
		return Configuration{}, err
	}

	return conf, nil
}
