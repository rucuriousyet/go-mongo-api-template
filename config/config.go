package config

import (
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type Configuration struct {
	Database struct {
		URL string `json:"mongo_url" hcl:"url"`
	} `json:"-" hcl:"database"`

	HTTP struct {
		ListenAddress             string   `json:"listen-address" hcl:"listen_addr"`
		AccessControlAllowOrigin  []string `json:"access-control-allow-origin" hcl:"allow_origins"`
		AccessControlAllowMethods []string `json:"access-control-allow-origin" hcl:"allow_methods"`
		AccessControlAllowCreds   bool     `json:"access-control-allow-creds" hcl:"allow_creds"`
		AccessControlDebug        bool     `json:"access-control-debug" hcl:"debug"`
	} `json:"http" hcl:"http"`

	Logger struct {
		ModeJSON  bool `json:"mode_json" hcl:"mode_json"`
		ModeDebug bool `json:"mode_debug" hcl:"mode_debug"`
	} `json:"logger" hcl:"logger"`

	Version   string    `json:"version" hcl:"-"`
	StartTime time.Time `json:"start-time" hcl:"-"`

	Mailgun struct {
		URL    string `json:"url" hcl:"url"`
		Key    string `json:"-" hcl:"key"`
		Sender string `json:"sender" hcl:"sender"`
	} `json:"mailgun" hcl:"mailgun"`

	Auth struct {
		Key string `json:"-" hcl:"key"`
	} `json:"-" hcl:"auth"`
}

var config Configuration
var logger zerolog.Logger

func GetConfig(configFile ...string) (Configuration, error) {
	if len(configFile) > 0 {
		confStruct, err := ReadFile(configFile[0])
		if err != nil {
			return Configuration{}, err
		}

		config = confStruct

		config.Version = "0.0.1"
		config.StartTime = time.Now()

		if !config.Logger.ModeJSON {
			logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
		} else {
			logger = zerolog.New(os.Stderr)
		}

		if !config.Logger.ModeDebug {
			logger = logger.Level(zerolog.WarnLevel)
		} else {
			logger = logger.Level(zerolog.DebugLevel)
		}
	}

	return config, nil
}

func GetLogger() zerolog.Logger {
	return logger
}
